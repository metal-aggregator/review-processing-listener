package com.ryan.amg.review.processing.listener.delegate

import com.ryan.amg.review.processing.listener.config.ServiceConfigurationProperties
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

class ReviewDelegateSpec extends Specification {

    RestTemplate mockRestTemplate = Mock()
    ServiceConfigurationProperties mockServiceConfigurationProperties = Mock()

    ReviewDelegate reviewDelegate = new ReviewDelegate(mockRestTemplate, mockServiceConfigurationProperties)

    def "Calling markReviewAsProcessed() calls the review-service API for marking the review as fully processed"() {
        given:
            1 * mockServiceConfigurationProperties.getReviewService() >> 'http://blarg.com'

            1 * mockRestTemplate.put('http://blarg.com/api/reviews/{reviewId}/processed/{processed}', null, 'reviewId-123', true)

        when:
            reviewDelegate.markReviewAsProcessed('reviewId-123')

        then:
            noExceptionThrown()
    }

}
