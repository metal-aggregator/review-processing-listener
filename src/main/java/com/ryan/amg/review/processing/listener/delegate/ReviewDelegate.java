package com.ryan.amg.review.processing.listener.delegate;

import com.ryan.amg.review.processing.listener.config.ServiceConfigurationProperties;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@AllArgsConstructor
public class ReviewDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(ReviewDelegate.class);
    private static final String REVIEW_SERVICE_ADD_GENRE_URI = "/api/reviews/{reviewId}/processed/{processed}";

    private final RestTemplate restTemplate;
    private final ServiceConfigurationProperties serviceConfigurationProperties;

    public void markReviewAsProcessed(String reviewId) {
        LOG.info("Attempting to mark reviewId={} as processed", reviewId);
        String targetUri = serviceConfigurationProperties.getReviewService() + REVIEW_SERVICE_ADD_GENRE_URI;
        restTemplate.put(targetUri, null, reviewId, true);
    }



}
