package com.ryan.amg.review.processing.listener.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewProcessing {
    private String reviewId;
    private List<String> unprocessedTags;
}
