package com.ryan.amg.review.processing.listener.domain

import com.ryan.amg.review.processing.listener.domain.aws.EventType

class ReviewProcessingStreamRecordBuilder {
    EventType operation = EventType.INSERT
    ReviewProcessing beforeReviewProcessingRecord = new ReviewProcessingBuilder().build()
    ReviewProcessing afterReviewProcessingRecord = new ReviewProcessingBuilder().build()

    ReviewProcessingStreamRecordBuilder withOperation(EventType operation) {
        this.operation = operation
        return this
    }

    ReviewProcessingStreamRecordBuilder withBeforeReviewProcessingRecord(ReviewProcessing beforeReviewProcessingRecord) {
        this.beforeReviewProcessingRecord = beforeReviewProcessingRecord
        return this
    }

    ReviewProcessingStreamRecordBuilder withAfterReviewProcessingRecord(ReviewProcessing afterReviewProcessingRecord) {
        this.afterReviewProcessingRecord = afterReviewProcessingRecord
        return this
    }

    ReviewProcessingStreamRecord build() {
        return new ReviewProcessingStreamRecord(
            operation: operation,
            beforeReviewProcessingRecord: beforeReviewProcessingRecord,
            afterReviewProcessingRecord: afterReviewProcessingRecord
        )
    }
}
