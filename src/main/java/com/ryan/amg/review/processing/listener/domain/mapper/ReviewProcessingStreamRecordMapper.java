package com.ryan.amg.review.processing.listener.domain.mapper;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.Record;
import com.ryan.amg.review.processing.listener.domain.ReviewProcessing;
import com.ryan.amg.review.processing.listener.domain.ReviewProcessingStreamRecord;
import com.ryan.amg.review.processing.listener.domain.aws.EventType;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ReviewProcessingStreamRecordMapper {

    private static final String KEY_REVIEW_ID = "reviewId";
    private static final String KEY_UNPROCESSED_IDS = "unprocessed_tags";

    public ReviewProcessingStreamRecord map(Record record) {
        EventType eventType = EventType.valueOf(record.getEventName());
        ReviewProcessing beforeReviewProcessing = mapReviewProcessingFromSource(record.getDynamodb().getOldImage());
        ReviewProcessing afterReviewProcessing = mapReviewProcessingFromSource(record.getDynamodb().getNewImage());
        return new ReviewProcessingStreamRecord(eventType, beforeReviewProcessing, afterReviewProcessing);
    }

    public ReviewProcessing mapReviewProcessingFromSource(Map<String, AttributeValue> sourceMap) {
        if (sourceMap == null) {
            return null;
        }
        String reviewId = sourceMap.get(KEY_REVIEW_ID).getS();
        List<String> unprocessedTagIds = sourceMap.get(KEY_UNPROCESSED_IDS).getL().stream().map(AttributeValue::getS).collect(Collectors.toList());

        return new ReviewProcessing(reviewId, unprocessedTagIds);
    }

}
