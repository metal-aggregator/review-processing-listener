Sample input:
```json
{
    "records": [
        {
            "eventID": "12345",
            "eventName": "MODIFY",
            "eventVersion": "1.1",
            "eventSource": "aws:dynamodb",
            "awsRegion": "us-east-2",
            "dynamodb": {
                "approximateCreationDateTime": "Jan 19, 1970, 2:11:08 PM",
                "keys": {
                    "reviewId": {
                        "s": "19b1013d-e8de-4d27-b967-dc85e7b2d66b"
                    }
                },
                "newImage": {
                    "unprocessed_tags": {
                        "l": [
                            {
                                "s": "2020"
                            },
                            {
                                "s": "Black Metal"
                            },
                            {
                                "s": "Burzum"
                            }
                        ]
                    },
                    "reviewId": {
                        "s": "19b1013d-e8de-4d27-b967-dc85e7b2d66b"
                    }
                },
                "oldImage": {
                    "unprocessed_tags": {
                        "l": [
                            {
                                "s": "Black Metal"
                            },
                            {
                                "s": "Burzum"
                            }
                        ]
                    },
                    "reviewId": {
                        "s": "19b1013d-e8de-4d27-b967-dc85e7b2d66b"
                    }
                },
                "sequenceNumber": "623745900000000003160435795",
                "sizeBytes": 478,
                "streamViewType": "NEW_AND_OLD_IMAGES"
            }
        }
    ]
}
```

Sample output:
```
Success
```

Environment variables:
* REVIEW_SERVICE_URL - Default to http://10.0.75.1:6661

The above defaults are from my local machine. See the Docker NAT ip4v entry from running `ipconfig`