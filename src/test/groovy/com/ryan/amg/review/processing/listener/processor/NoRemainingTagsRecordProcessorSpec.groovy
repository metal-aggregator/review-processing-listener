package com.ryan.amg.review.processing.listener.processor

import com.ryan.amg.review.processing.listener.delegate.ReviewDelegate
import com.ryan.amg.review.processing.listener.domain.ReviewProcessing
import com.ryan.amg.review.processing.listener.domain.ReviewProcessingBuilder
import com.ryan.amg.review.processing.listener.domain.ReviewProcessingStreamRecord
import com.ryan.amg.review.processing.listener.domain.ReviewProcessingStreamRecordBuilder
import com.ryan.amg.review.processing.listener.domain.aws.EventType
import spock.lang.Specification
import spock.lang.Unroll

class NoRemainingTagsRecordProcessorSpec extends Specification {

    ReviewDelegate mockReviewDelegate = Mock()

    NoRemainingTagsRecordProcessor noRemainingTagsRecordProcessor = new NoRemainingTagsRecordProcessor(mockReviewDelegate)

    @Unroll
    def "Calling supports() returns #expectedResult when the record eventType=#inputEventType and unprocessedTags #description"() {
        given:
            ReviewProcessing inputAfterRecord = new ReviewProcessingBuilder().withUnprocessedTags(inputTags).build()
            ReviewProcessingStreamRecord inputRecord = new ReviewProcessingStreamRecordBuilder().withOperation(inputEventType).withAfterReviewProcessingRecord(inputAfterRecord).build()

        when:
            boolean actualResult = noRemainingTagsRecordProcessor.supports(inputRecord)

        then:
            expectedResult == actualResult

        where:
            description    | expectedResult | inputEventType   | inputTags
            'is empty'     | false          | EventType.INSERT | []
            'is empty'     | true           | EventType.MODIFY | []
            'is empty'     | false          | EventType.REMOVE | []
            'is not empty' | false          | EventType.INSERT | ['x']
            'is not empty' | false          | EventType.MODIFY | ['x']
            'is not empty' | false          | EventType.REMOVE | ['x']
    }

    def "Calling process invokes the review delegate"() {
        given:
            ReviewProcessing afterRecord = new ReviewProcessingBuilder().withReviewId('someAfterId-123').build()
            ReviewProcessingStreamRecord inputRecord = new ReviewProcessingStreamRecordBuilder().withAfterReviewProcessingRecord(afterRecord).build()

            1 * mockReviewDelegate.markReviewAsProcessed('someAfterId-123')

        when:
            noRemainingTagsRecordProcessor.process(inputRecord)

        then:
            noExceptionThrown()
    }

}
