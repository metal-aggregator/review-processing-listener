package com.ryan.amg.review.processing.listener.processor;

import com.ryan.amg.review.processing.listener.domain.ReviewProcessingStreamRecord;

public interface RecordProcessor {
    boolean supports(ReviewProcessingStreamRecord reviewProcessingStreamRecord);
    void process(ReviewProcessingStreamRecord reviewProcessingStreamRecord);
}
