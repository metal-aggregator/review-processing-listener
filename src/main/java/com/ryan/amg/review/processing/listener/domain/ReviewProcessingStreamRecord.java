package com.ryan.amg.review.processing.listener.domain;

import com.ryan.amg.review.processing.listener.domain.aws.EventType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewProcessingStreamRecord {
    private EventType operation;
    private ReviewProcessing beforeReviewProcessingRecord;
    private ReviewProcessing afterReviewProcessingRecord;
}
