package com.ryan.amg.review.processing.listener.service;

import com.amazonaws.services.dynamodbv2.model.Record;
import com.google.gson.Gson;
import com.ryan.amg.review.processing.listener.domain.ReviewProcessingStreamRecord;
import com.ryan.amg.review.processing.listener.domain.mapper.ReviewProcessingStreamRecordMapper;
import com.ryan.amg.review.processing.listener.processor.RecordProcessor;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class ReviewProcessingListenerService {

    private static final Logger LOG = LoggerFactory.getLogger(ReviewProcessingListenerService.class);

    private final List<RecordProcessor> recordProcessors;
    private final ReviewProcessingStreamRecordMapper reviewProcessingStreamRecordMapper;

    public void applyTagStreamRecords(List<Record> rawTagStreamRecords) {
        rawTagStreamRecords.stream().map(reviewProcessingStreamRecordMapper::map).forEach(this::applyTagStreamRecord);
    }

    private void applyTagStreamRecord(ReviewProcessingStreamRecord reviewProcessingStreamRecord) {
        LOG.info("Attempting to apply record processors to reviewProcessingStreamRecord={}", new Gson().toJson(reviewProcessingStreamRecord));
        recordProcessors.stream().filter(k -> k.supports(reviewProcessingStreamRecord)).forEach(k -> k.process(reviewProcessingStreamRecord));
    }

}
