package com.ryan.amg.review.processing.listener.function;

import com.google.gson.Gson;
import com.ryan.amg.review.processing.listener.domain.RecordsWrapper;
import com.ryan.amg.review.processing.listener.domain.mapper.RecordsWrapperMapper;
import com.ryan.amg.review.processing.listener.service.ReviewProcessingListenerService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.function.Function;

@Component("com.ryan.amg.review.processing.listener.handler.aws.ReviewProcessingListenerHandler")
@AllArgsConstructor
public class ReviewProcessingListenerFunction implements Function<InputStream, Object> {

    private static final Logger LOG = LoggerFactory.getLogger(ReviewProcessingListenerFunction.class);

    private final RecordsWrapperMapper recordsWrapperMapper;
    private final ReviewProcessingListenerService reviewProcessingListenerService;

    @Override
    public Object apply(InputStream inputStream) {
        RecordsWrapper recordsWrapper = recordsWrapperMapper.map(inputStream);
        LOG.info("Incoming tag write event: " + new Gson().toJson(recordsWrapper));
        reviewProcessingListenerService.applyTagStreamRecords(recordsWrapper.getRecords());
        return "Success";
    }

}
