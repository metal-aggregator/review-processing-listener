package com.ryan.amg.review.processing.listener.function

import com.amazonaws.services.dynamodbv2.model.Record
import com.ryan.amg.review.processing.listener.domain.RecordsWrapper
import com.ryan.amg.review.processing.listener.domain.StreamRecordBuilder
import com.ryan.amg.review.processing.listener.domain.mapper.RecordsWrapperMapper
import com.ryan.amg.review.processing.listener.service.ReviewProcessingListenerService
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class ReviewProcessingListenerFunctionSpec extends Specification {

    RecordsWrapperMapper mockRecordsWrapperMapper = Mock()
    ReviewProcessingListenerService mockReviewProcessingListenerService = Mock()

    ReviewProcessingListenerFunction reviewProcessingListenerFunction = new ReviewProcessingListenerFunction(mockRecordsWrapperMapper, mockReviewProcessingListenerService)

    def "Invoking apply() calls mapper and passes the result into the ReviewProcessingListenerService to be processed"() {

        given:
            InputStream inputInputStream = new ByteArrayInputStream('Input data'.getBytes())

            InputStream expectedMapperInputStream = new ByteArrayInputStream('Input data'.getBytes())
            RecordsWrapper mockedRecordsWrapper = new RecordsWrapper(records: [new StreamRecordBuilder().build()])
            RecordsWrapper expectedRecordsWrapper = new RecordsWrapper(records: [new StreamRecordBuilder().build()])

            1 * mockRecordsWrapperMapper.map(_ as InputStream) >> { args ->
                assertReflectionEquals(expectedMapperInputStream, args.get(0))
                return mockedRecordsWrapper
            }

            1 * mockReviewProcessingListenerService.applyTagStreamRecords(_ as List<Record>) >> { args ->
                assertReflectionEquals(expectedRecordsWrapper.records, args.get(0))
            }

        when:
            String actualResult = reviewProcessingListenerFunction.apply(inputInputStream)

        then:
            actualResult == 'Success'

    }

}