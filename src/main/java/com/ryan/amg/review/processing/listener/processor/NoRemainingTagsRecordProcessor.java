package com.ryan.amg.review.processing.listener.processor;

import com.ryan.amg.review.processing.listener.delegate.ReviewDelegate;
import com.ryan.amg.review.processing.listener.domain.ReviewProcessingStreamRecord;
import com.ryan.amg.review.processing.listener.domain.aws.EventType;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
@AllArgsConstructor
public class NoRemainingTagsRecordProcessor implements RecordProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(NoRemainingTagsRecordProcessor.class);

    private final ReviewDelegate reviewDelegate;

    @Override
    public boolean supports(ReviewProcessingStreamRecord reviewProcessingStreamRecord) {
        return isModifyOperation(reviewProcessingStreamRecord) && isNoRemainingTagsToProcess(reviewProcessingStreamRecord);
    }

    @Override
    public void process(ReviewProcessingStreamRecord reviewProcessingStreamRecord) {
        LOG.info("Marking reviewId={} as fully processed.", reviewProcessingStreamRecord.getAfterReviewProcessingRecord().getReviewId());
        reviewDelegate.markReviewAsProcessed(reviewProcessingStreamRecord.getAfterReviewProcessingRecord().getReviewId());
    }

    private boolean isModifyOperation(ReviewProcessingStreamRecord reviewProcessingStreamRecord) {
        return reviewProcessingStreamRecord.getOperation() == EventType.MODIFY;
    }

    private boolean isNoRemainingTagsToProcess(ReviewProcessingStreamRecord reviewProcessingStreamRecord) {
        return CollectionUtils.isEmpty(reviewProcessingStreamRecord.getAfterReviewProcessingRecord().getUnprocessedTags());
    }

}
