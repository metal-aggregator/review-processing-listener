package com.ryan.amg.review.processing.listener.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

/*
 * As of 7/10/2020, AWS DynamoDB stream events represent their timestamps as scientific-notation decimals (e.g. "ApproximateCreationDateTime":1.594385176E9.
 * This representation makes the default deserializer choke. This class is a custom deserializer to handle this jankery.
 */
public class DateDeserializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(jsonParser.getValueAsLong());
        return calendar.getTime();
    }
}
