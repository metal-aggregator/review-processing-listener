package com.ryan.amg.review.processing.listener.domain.aws;

public enum EventType {
    MODIFY,
    INSERT,
    REMOVE
}
