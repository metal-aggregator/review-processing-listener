package com.ryan.amg.review.processing.listener.domain;

import com.amazonaws.services.dynamodbv2.model.Record;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class RecordsWrapper {

    private List<Record> records = new ArrayList<>();

    @JsonProperty("Records")
    public RecordsWrapper setRecords(List<Record> records) {
        this.records = records;
        return this;
    }

}
