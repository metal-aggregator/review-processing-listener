call sam build ReviewProcessingListener ^
--template "C:\Dev\Projects\Metal Aggregator\Modules\review-processing-listener\CICD\template.yml" ^
--build-dir "C:\Dev\Projects\Metal Aggregator\Modules\review-processing-listener\build\.aws-sam\build" ^
--base-dir "C:\Dev\Projects\Metal Aggregator\Modules"

call sam package ^
--template-file "C:\Dev\Projects\Metal Aggregator\Modules\review-processing-listener\build\.aws-sam\build\template.yaml" ^
--s3-bucket ryan-sdg-metalaggregator-lambda ^
--output-template-file "C:\Dev\Projects\Metal Aggregator\Modules\review-processing-listener\build\.aws-sam\build\packaged-template.yml"

call sam deploy ^
--template-file "C:\Dev\Projects\Metal Aggregator\Modules\review-processing-listener\build\.aws-sam\build\packaged-template.yml" ^
--stack-name ReviewProcessingListenerLambdaStack ^
--capabilities CAPABILITY_IAM ^
--region us-east-2 ^
--s3-bucket ryan-sdg-metalaggregator-lambda ^
--no-fail-on-empty-changeset