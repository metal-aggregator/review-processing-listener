package com.ryan.amg.review.processing.listener.domain.mapper

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.gson.Gson
import com.ryan.amg.review.processing.listener.domain.RecordsWrapper
import com.ryan.amg.review.processing.listener.domain.StreamRecordBuilder
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class RecordsWrapperMapperSpec extends Specification {

    ObjectMapper mockObjectMapper = Mock()

    RecordsWrapperMapper recordsWrapperMapper = new RecordsWrapperMapper(mockObjectMapper)

    def "Calling map() with a well-formed and parseable InputStream results in the expected RecordsWrapper"() {

        given:
            RecordsWrapper inputRecordsWrapper = new RecordsWrapper([new StreamRecordBuilder().build()])
            InputStream inputInputStream = new ByteArrayInputStream(new Gson().toJson(inputRecordsWrapper).getBytes())
            byte [] expectedBytes = new Gson().toJson(inputRecordsWrapper).getBytes()

            RecordsWrapper mockedRecordsWrapper = new RecordsWrapper([new StreamRecordBuilder().build()])
            RecordsWrapper expectedRecordsWrapper = new RecordsWrapper([new StreamRecordBuilder().build()])

            1 * mockObjectMapper.readValue(_ as byte[], RecordsWrapper.class) >> { args ->
                assertReflectionEquals(expectedBytes, args.get(0))
                return mockedRecordsWrapper
            }

        when:
            RecordsWrapper actualRecordsWrapper = recordsWrapperMapper.map(inputInputStream)

        then:
            assertReflectionEquals(expectedRecordsWrapper, actualRecordsWrapper)

    }

    def "Calling map() throws a RuntimeException when parsing the InputStream results in an IOException"() {

        given:
            InputStream inputInputStream = new ByteArrayInputStream()

            1 * mockObjectMapper.readValue(_ as byte[], RecordsWrapper.class) >> { args ->
                throw new IOException('Test')
            }

        when:
            recordsWrapperMapper.map(inputInputStream)

        then:
            RuntimeException actualRuntimeException = thrown(RuntimeException.class)
            actualRuntimeException.message == 'Unable to parse input stream.'

    }

}
