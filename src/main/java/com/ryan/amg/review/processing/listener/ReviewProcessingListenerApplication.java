package com.ryan.amg.review.processing.listener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReviewProcessingListenerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ReviewProcessingListenerApplication.class, args);
    }
}
