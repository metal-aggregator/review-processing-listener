package com.ryan.amg.review.processing.listener.domain

class ReviewProcessingBuilder {
    String reviewId = 'ReviewId-123'
    List<String> unprocessedTags = ['tag1', 'tag2']

    ReviewProcessingBuilder withReviewId(String reviewId) {
        this.reviewId = reviewId
        return this
    }

    ReviewProcessingBuilder withUnprocessedTags(List<String> unprocessedTags) {
        this.unprocessedTags = unprocessedTags
        return this
    }

    ReviewProcessing build() {
        return new ReviewProcessing(
            reviewId: reviewId,
            unprocessedTags: unprocessedTags
        )
    }

}
