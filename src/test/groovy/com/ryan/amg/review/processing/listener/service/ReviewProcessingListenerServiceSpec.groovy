package com.ryan.amg.review.processing.listener.service

import com.amazonaws.services.dynamodbv2.model.Record
import com.ryan.amg.review.processing.listener.domain.ReviewProcessingStreamRecord
import com.ryan.amg.review.processing.listener.domain.ReviewProcessingStreamRecordBuilder
import com.ryan.amg.review.processing.listener.domain.StreamRecordBuilder
import com.ryan.amg.review.processing.listener.domain.mapper.ReviewProcessingStreamRecordMapper
import com.ryan.amg.review.processing.listener.processor.RecordProcessor
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class ReviewProcessingListenerServiceSpec extends Specification {

    def "Calling applyTagStreamRecords calls the mapper and applies processors that support the records"() {
        given:
            ReviewProcessingStreamRecordMapper mockRecordMapper = Mock()
            Record inputRecord = new StreamRecordBuilder().withBeforeTagName('beforeTag').withBeforeReviewIds(['id1', 'id2']).withAfterTagName('afterTag').withAfterReviewIds(['id3', 'id4']).build()
            Record expectedRecord = new StreamRecordBuilder().withBeforeTagName('beforeTag').withBeforeReviewIds(['id1', 'id2']).withAfterTagName('afterTag').withAfterReviewIds(['id3', 'id4']).build()

            ReviewProcessingStreamRecord mockedStreamRecord = new ReviewProcessingStreamRecordBuilder().build()
            ReviewProcessingStreamRecord expectedStreamRecord = new ReviewProcessingStreamRecordBuilder().build()

            RecordProcessor mockRecordProcessor1 = Mock()
            RecordProcessor mockRecordProcessor2 = Mock()

            1 * mockRecordMapper.map(_ as Record) >> { args ->
                assertReflectionEquals(expectedRecord, (Record) args.get(0))
                return mockedStreamRecord
            }

            1 * mockRecordProcessor1.supports(_ as ReviewProcessingStreamRecord) >> { args ->
                assertReflectionEquals(expectedStreamRecord, (ReviewProcessingStreamRecord) args.get(0))
                return false
            }

            0 * mockRecordProcessor1.process(_ as ReviewProcessingStreamRecord)

            1 * mockRecordProcessor2.supports(_ as ReviewProcessingStreamRecord) >> { args ->
                assertReflectionEquals(expectedStreamRecord, (ReviewProcessingStreamRecord) args.get(0))
                return true
            }

            1 * mockRecordProcessor2.process(_ as ReviewProcessingStreamRecord) >> { args ->
                assertReflectionEquals(expectedStreamRecord, (ReviewProcessingStreamRecord) args.get(0))
            }

            ReviewProcessingListenerService reviewProcessingListenerService = new ReviewProcessingListenerService([mockRecordProcessor1, mockRecordProcessor2], mockRecordMapper)

        when:
            reviewProcessingListenerService.applyTagStreamRecords([inputRecord])

        then:
            noExceptionThrown()
    }

}
